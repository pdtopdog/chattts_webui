#安装步骤：


conda create -n chatTTS_evn python=3.10

conda activate chatTTS_evn

conda update -n base -c defaults conda

pip install torch==2.2.0 torchaudio==2.2.0 --index-url https://download.pytorch.org/whl/cu118

pip install nvidia-cublas-cu11 nvidia-cudnn-cu11

#git clone https://gitlab.com/pdtopdog/chattts_webui.git chat-tts-ui

pip install -r requirements.txt

conda deactivate
